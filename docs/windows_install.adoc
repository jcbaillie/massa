# Install and Run on Windows

## Set up your Rust environment

* On Windows, you should first follow the indications from Microsoft to be able to run on a Rust environment link:https://docs.microsoft.com/en-gb/windows/dev-environment/rust/setup[here].
 ** Install Visual Studio (recommended) or the Microsoft C++ Build Tools
 ** Once Visual Studio is installed, click on C++ Build Tool. Select on the right column called “installation details” the following packages: 
  *** MSCV v142 – VS 2019
  *** Windows 10 SDK
  *** C++ CMake tools for Windows
  *** Testing Tools Core Feature
 ** Click install on the bottom right to download & install those packages
* Install Rust, to be downloaded link:https://www.rust-lang.org/tools/install[here]
* Install Visual Studio Code, to be downloaded link:https://code.visualstudio.com/[here]
 ** Install the Rust Analyzer extension for VSC, link:https://marketplace.visualstudio.com/items?itemName=matklad.rust-analyzer[here]
 ** Install the CodeLLDB extension for VSC, link:https://marketplace.visualstudio.com/items?itemName=vadimcn.vscode-lldb[here]


# Download the Massa Git Repository 

* Download the Massa Git Repository here (in ZIP): link:https://gitlab.com/massalabs/massa[Massa].
* Extract the ZIP.


# Launch your Node

* Open Visual Studio Code
* Open the VSC Terminal
 ** Type: `rustup default nightly`
 ** Type: `cd /path/to/folder`
 ** Type: `cd /massa-node`
 ** Type: `cargo run --release`

Once the process is finished, you should leave the VSC Terminal opened. 

Your node is running. 

You can now move toward the wallet creation (using Powershell or the Classic Terminal), and then the staking process. 

